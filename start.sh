#!/bin/sh

env

set -x

set_val() {
    if [ -n "$2" ]; then
        echo "set '$2' to '$1'"
        sed -i "s/<$2>[^<]*<\/$2>/<$2>$1<\/$2>/g" /etc/icecast2/icecast.xml
    else
        echo "Setting for '$1' is missing, skipping." >&2
    fi
}

set_mounts() {
    if [ -n "$1" ]; then
        for USERNAME in $(echo "${ICECAST_STREAMERS_CREDS_JSON}" | jq -r '. | keys[]'); do
            PASSWORD=$(echo "${ICECAST_STREAMERS_CREDS_JSON}" | jq -r ".${USERNAME}")
            envsubst > insert-content <<EOF

<mount type="normal">
    <mount-name>/${USERNAME}</mount-name>
    <username>${USERNAME}</username>
    <password>${PASSWORD}</password>
    <!-- <dump-file>/recordings/${USERNAME}-%Y-%m-%d-%k-%M.ogg</dump-file> -->
</mount>

EOF
            cat insert-content
            sed -i "/<!-- ICECAST_MOUNTS -->/r insert-content" /etc/icecast2/icecast.xml
            rm -f insert-content
        done;
    else
        echo "Setting for '$1' is missing, skipping." >&2
    fi
}

set_val $ICECAST_SOURCE_PASSWORD source-password
set_val $ICECAST_RELAY_PASSWORD  relay-password
set_val $ICECAST_ADMIN_PASSWORD  admin-password
set_val $ICECAST_HOSTNAME        hostname

set_mounts $ICECAST_STREAMERS_CREDS_JSON

set -e
exec sudo -Eu icecast2 icecast2 -n -c /etc/icecast2/icecast.xml