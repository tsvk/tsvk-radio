FROM debian:stable-slim

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -qq -y update && \
  apt-get -qq -y full-upgrade && \ 
  apt-get -qq -y install icecast2 python-setuptools gettext sudo jq cron-apt && \
  apt-get -y autoclean && \
  apt-get clean && \
  chown -R icecast2 /etc/icecast2 && \
  sed -i 's/ -d//' /etc/cron-apt/action.d/3-download 

CMD ["/start.sh"]
EXPOSE 8000
VOLUME ["/config", "/var/log/icecast2"]

COPY ./start.sh /start.sh
COPY ./etc /etc