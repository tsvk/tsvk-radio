# Icecast in Docker

Icecast2 Dockerfile

## Build

```bash
docker build -t icecast2-server .
```

## Run

Run with default password, export port 8000

```bash
docker run -p 8000:8000 icecast2-server
$BROWSER localhost:8000
```

Run with custom password

```bash
docker run -p 8000:8000 -e ICECAST_SOURCE_PASSWORD=aaaa -e ICECAST_ADMIN_PASSWORD=bbbb -e ICECAST_PASSWORD=cccc -e ICECAST_RELAY_PASSWORD=dddd -e ICECAST_HOSTNAME=noise.example.com icecast2-server
```

Run with custom configuration

```bash
docker run -p 8000:8000 -v /local/path/to/icecast/config:/etc/icecast2 icecast2-server
docker run -p 8000:8000 -v /local/path/to/icecast.xml:/etc/icecast2/icecast.xml icecast2-server
```

Docker-compose

```yaml
icecast:
  image: icecast2-server
  volumes:
    - logs:/var/log/icecast2
    - /etc/localtime:/etc/localtime:ro
  environment:
    - ICECAST_SOURCE_PASSWORD=aaa
    - ICECAST_ADMIN_PASSWORD=bbb
    - ICECAST_PASSWORD=ccc
    - ICECAST_RELAY_PASSWORD=ddd
    - ICECAST_HOSTNAME=noise.example.com
  ports:
    - 8000:8000
```